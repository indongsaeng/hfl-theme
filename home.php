<?php  
	/*
		Template Name: Home Page
	*/
?>

<?php get_header(); ?>
	<div id="home-container">
		<section class="upper-content">
			<h2>Sliders Main</h2>
		</section>
		<section class="mid-content">
			<div class="hlf-best-sellers">
				<h2 class="hlf-bestsellers-header hlf-home-blue-gradient">Best Sellers</h2>
				<div id="hlf-product-slider">
					
				</div>
			</div>
			<div class="side-navigation">
				<?php sec_nav(); ?>
			</div>
		</section>
		<section class="lower-content">
			<header class="about_header">
			<img class="about_pic" src="<?php echo get_template_directory_uri();?>/images/about.jpg" alt="about">
			<span class="about_top"><p>About</p><img src="<?php echo get_template_directory_uri();?>/images/logo.png" alt="logo"></span>
				Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
				tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
				quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
				consequat.
			</header>
		</section>
		<section>
			<ul class="blog_main">
				<li class="blog_cont1"><div class="blog_post">Cont 1</div><div class="blog_read">Read More >>></div></li>
				<li class="blog_cont2">Cont 2</li>
				<li class="blog_cont1">Cont 3</li>
				<li class="blog_cont2">Cont 4</li>
			</ul>
		</section>
	</div>
	<div class="clear"></div>
<?php get_footer(); ?>